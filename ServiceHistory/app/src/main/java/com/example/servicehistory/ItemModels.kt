package com.example.servicehistory

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ItemModels (
    @Expose
    @SerializedName("history")
    val records:List<Records>
)
data class Records(
    @Expose
    @SerializedName("date")
    val his_date:String,

    @Expose
    @SerializedName("month")
    val his_month:String,

    @Expose
    @SerializedName("amount")
    val his_amount:String,

    @Expose
    @SerializedName("notify")
    val his_notify:String
)