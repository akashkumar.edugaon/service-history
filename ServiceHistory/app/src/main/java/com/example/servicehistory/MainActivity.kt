package com.example.servicehistory

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),OnItemClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadApi()
    }
    fun loadApi(){
        val ApiService= RecyclerApi.create()

        ApiService.getItem()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({result->
                renderProduct(result.records)
            },{ error->
                error.printStackTrace()
            })
    }

    private fun renderProduct(productsList: List<Records>){
        recyclerView.layoutManager= LinearLayoutManager(this)
        recyclerView.adapter= RecyclerAdapter(productsList,this)
    }

    override fun OnClick(position: Int) {
        val  intent = Intent(this,HistoryActivity::class.java)
        startActivity(intent)
    }
}