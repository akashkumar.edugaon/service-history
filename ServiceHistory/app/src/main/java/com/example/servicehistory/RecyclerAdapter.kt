package com.example.servicehistory

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.history_list_item.view.*

class RecyclerAdapter(private val listItem:List<Records>,private val onItemClickListener: OnItemClickListener ):
    RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.history_list_item,parent,false)
        return RecyclerViewHolder(view,onItemClickListener)
    }

    override fun getItemCount() = listItem.size
    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val his_item = listItem[position]

        holder.view.date.text = his_item.his_date
        holder.view.amount.text = his_item.his_amount
        holder.view.month.text = his_item.his_month
        holder.view.notification.text = his_item.his_notify
    }

    class RecyclerViewHolder(val view: View,onItemClickListener: OnItemClickListener): RecyclerView.ViewHolder(view){
        init {
            view.setOnClickListener {
                onItemClickListener. OnClick(adapterPosition)
            }
        }

    }
}
interface OnItemClickListener{
   fun OnClick(position: Int)

}


