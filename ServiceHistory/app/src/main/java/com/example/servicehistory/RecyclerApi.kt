package com.example.servicehistory

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface RecyclerApi  {

    @GET("history")
    fun getItem(): Observable<ItemModels>
    companion object Factory{
        fun create():RecyclerApi{
            val retrofit= Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://demo4346101.mockable.io/")
                .build()
            return (retrofit.create(RecyclerApi::class.java))
        }
    }
}